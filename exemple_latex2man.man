'\" t
.\" Manual page created with latex2man on mardi 15 novembre 2016, 16:00:39 (UTC+0100)
.\" NOTE: This file is generated, DO NOT EDIT.
.de Vb
.ft CW
.nf
..
.de Ve
.ft R

.fi
..
.TH "GENERATEURPLAYLIST" "1" "15 November 2016" "Générateur de playlist " "Générateur de playlist "
.SH NAME

.PP
generateurPlaylist
est un outil permettant de générer des 
fichiers de playlist pour les lecteurs multimedia. Sa 
documentation est fournie au moins en page de manuel accessible à 
l\&'aide de la commande \fIman\fP(1)\&.
.PP
.SH SYNOPSIS

generateurPlaylist
\fB\-d\fP\fIdureetotale\fP
\fB\-\-genre\fP\fI youpi\fP
\fI50\fP
[\fB\-h\fP]
[\fB\-V\fP]
\fIsortieM3U\fP
.PP
generateurPlaylist
[\fB\-h\fP]
[\fB\-\-log\fP\fIDEBUG|INFO|WARNING|ERROR|CRITICAL\fP]
.PP
.SH DESCRIPTION

generateurPlaylist
sert à générer un fichier de playlist 
sortieM3U
au format M3U afin de blablabla. 
.PP
La description des différentes options se trouve dans la section 
suivante. 
.PP
.SH OPTIONS

.TP
\fB\-d\fP\fIdureetotale\fP
 Specifie la durée totale que devra 
faire la playlist. 
.TP
\fB\-\-genre\fP\fINOM QUANTITÉ\fP
 Permet de définir le ou les 
genres à prendre en compte. Le formalisme correspond à 
blablablabla. 
.TP
\fB\-h\fP
 Affiche un message d\&'aide. 
.TP
\fB\-V\fP
 Affiche les informations de version. 
.PP
.SH FICHIERS

.TP
/usr/local/bin/generateurPlaylist
 Le programme 
principal. 
.TP
/etc/generateurPlaylist.cfg
 Le fichier de 
configuration du programme. 
.PP
.SH À VOIR AUSSI

\fIpython3\fP(1),
\fIvlc\fP(1),
\fImp3rename\fP(1)\&.
.PP
.SH TRADUCTION

L\&'application a été écrite en langue anglaise, traduite en 
français. Nous recherchons des traducteurs dans les langues : English, 
German, Italian, Spanish, Russian, Chinese, Japanese. 
.PP
.SH PRé\-REQUIS

.TP
python3 
generateurPlaylist
a besoin de python
version >= 3.2 
.TP
setuptools 
Si vous souhaitez installer proprement 
l\&'application, il est intéressant de disposer du programme 
python3\-setuptools\&.
.PP
.SH VERSION

Version : 0.1.2  du 15 November 2016\&.
.PP
.SH LICENCE ET COPYRIGHT

.TP
Copyright 
(C)15 November 2016,Grégory DAVID, 3 rue Beau Soleil, 72700 Allonnes 
.TP
Licence 
Indiquer les termes succins de la licence d\&'utilisation. 
.PP
.TP
Misc 
If you find this software useful, please send me a postcard. 
.PP
.SH AUTEUR

Grégory DAVID 
.br
3 rue Beau Soleil 
.br
72700 Allonnes 
.br
e\-mail : \fBgregory.david@ac\-nantes.fr\fP
.br
www : \fBhttp://www.bts\-malraux72.net\fP\&.
.\" NOTE: This file is generated, DO NOT EDIT.
